<meta name="viewport" content="width=device-width", user-scalable="no">
<?php
/**
 * Plugin Name: Recensioni Plugin
 * Plugin URI:
 * Description: Plugin per visualizzare i custom post type per lo slider recensioni di Tilas.
 * Author: Daniele Trisolino
 * Author URI:
 * Version: 1.0
 *
*/
/* ------------------------------------------------------------------------- *
*   CUSTOM POST TYPE Recensioni
/* ------------------------------------------------------------------------- */

    add_action('init', 'recensioni');
    function recensioni() {
        $labels = array(
            'name'               => __('Recensioni' , 'recensioni-plugin'),
            'singular_name'      => __('Recensione' , 'recensioni-plugin'),
            'add_new'            => __('Aggiungi recensione', 'recensioni-plugin'),
            'add_new_item'       => __('Aggiungi nuova recensione' , 'recensioni-plugin'),
            'edit_item'          => __('Modifica recensione', 'recensioni-plugin'),
            'new_item'           => __('Nuova recensione', 'recensioni-plugin'),
            'all_items'          => __('Tutte le recensioni', 'recensioni-plugin'),
            'view_item'          => __('Mostra recensioni' , 'recensioni-plugin'),
            'search_items'       => __('Cerca recensioni' , 'recensioni-plugin'),
            'not_found'          => __('Recensioni non trovate', 'recensioni-plugin'),
            'not_found_in_trash' => __('Recensioni non trovate nel cestino', 'recensioni-plugin'),
        );
        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'rewrite'            => array('slug' => 'recensioni'),
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => 22,
            'menu_icon'          => 'dashicons-universal-access',
            'supports'           => array(
                'title',
                'thumbnail',
            ),
        );
        register_post_type('recensioni', $args);
    }

/*------------------------------------------------------------------------- *
*   Shortcode Recensioni
* -------------------------------------------------------------------------*/

    add_shortcode('recensioni', 'recensione');
    function recensione($atts)
    {
        ob_start(); ?>

    <?php
    function jquery() {
        wp_enqueue_script('jquery');
    }
    add_action ('wp_enqueue_script', 'jquery');

    function touchSwipe () {
        wp_register_script('jquery.touchSwipe.min', get_template_directory_uri().'js/jquery.touchSwipe.min.js');
        wp_enqueue_script ('jquery.touchSwipe.min');
    }
    add_action( 'wp_enqueue_scripts', 'touchSwipe' );?>


    <script>
    jQuery(document).ready(function(){
        var speed = 500; //transition speed - fade
        var autoswitch = false; //auto slider options
        var autoswitch_speed = 5000; //auto slider speed
        jQuery('.swiper-slide').first().addClass('active');
        jQuery('.active').show();
        jQuery('img.dopo').on('click', nextSlide);// call function nextSlide
        jQuery('img.prima').on('click', prevSlide);// call function prevSlide
        function nextSlide(){
            jQuery('.active').removeClass('active').addClass('oldActive');
                if(jQuery('.oldActive').is(':last-child')){
                    jQuery('.swiper-slide').first().addClass('active');
                } else {
                    jQuery('.oldActive').next().addClass('active');
                }
            jQuery('.oldActive').removeClass('oldActive');
            jQuery('.swiper-slide').fadeOut(speed);
            jQuery('.active').fadeIn(speed);
        }
        function prevSlide(){
            jQuery('.active').removeClass('active').addClass('oldActive');
                if(jQuery('.oldActive').is(':first-child')){
                    jQuery('.swiper-slide').last().addClass('active');
                } else {
                    jQuery('.oldActive').prev().addClass('active');
                }
            jQuery('.oldActive').removeClass('oldActive');
            jQuery('.swiper-slide').fadeOut(speed);
            jQuery('.active').fadeIn(speed);
        }
    });
    /*jQuery(document).ready(function() {
        jQuery('.swiper-slide').first().addClass('active');
        jQuery('.active').show();
        jQuery(".swiper-container").swipe( {
            //Generic swipe handler for all directions
            swipeLeft:function(event, direction, distance, duration, fingerCount) {
                jQuery('.swiper-slide').fadeOut(500);
                jQuery('.active').fadeIn(500)
            },
            swipeRight: function() {
                jQuery('.swiper-slide').fadeOut(500);
                jQuery('.active').fadeIn(500)
            },
            //Default is 75px, set to 0 for demo so any distance triggers swipe
            threshold:0
        });
    });*/
    </script>


    <style>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,300,600);

    /* Slider */
        div.swiper-container {
            position: relative;
            overflow: hidden;
            margin: 20px auto 0 auto;
            list-style: none;
            min-height: 514px;
            width: 100%;
            background-image: url("wp-content/plugins/recensioni-plugin/img/testimonianze-background.png");
            z-index: 1;
        }

        .swiper-wrapper {
            position: relative;
            overflow: hidden;
            margin: 0;
            padding: 0;
            height: 500px;
            list-style: none;
            width: 100%;
        }

        .swiper-slide {
            position: relative;
            display: block;
            float: left;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 500px;
            text-align: center;
            line-height: 0px;
            list-style: none;
        }

    /* Immagini fisse dello slider */
        img.prima, img.dopo {
            position: absolute;
            top: 40%;
            z-index: 999;
            display: block;
            padding: 1.4% 3.6%;
            cursor: pointer;
        }

        img.dopo {
            right: 0;
        }

        img.apice {
            position: absolute;
            top: 10%;
            left: 29%;
            z-index: 999;
            display: block;
            padding: 1.2% 3%;
        }

        img.pedice {
            position: absolute;
            top: 62.7%;
            left: 42.4%;
            z-index: 999;
            display: block;
            padding: 4% 3%;
        }

        img.apice2 {
            position: absolute;
            top: 10%;
            left: 58.8%;
            z-index: 999;
            display: block;
            padding: 1.2% 3%;
        }

        img.pedice2 {
            position: absolute;
            top: 62.7%;
            left: 72.2%;
            z-index: 999;
            display: block;
            padding: 4% 3%;
        }

    /* Riquadro */

        div.quadro {
            margin-left: 17.3%;
            margin-top: -24.8%;
            padding-top: 18%;
            height: 70%;
        }

        div#testo-primo-riquadro {
            text-align: center;
            font-size: 114%;
            float: left;
            text-transform: uppercase;
            width: 8%;
            margin-top: 18%;
            line-height: 19px;
            color: #fff;
            font-family: "Open Sans";
            margin-right: 2%;
            margin-left: 8%;
        }

        div#testo-secondo-riquadro {
            text-align: left;
            font-size: 140%;
            float: left;
            display: block;
            width: 18%;
            line-height: 30px;
            margin-top: 16%;
            color: #fff;
            font-family: "Open Sans";
        }

        div#testo-primo-riquadro img {
            height: 128px;
            width: 128px;
            border-radius: 100%;
            margin-bottom: 13%;
        }

    /*Media Query*/

        @media screen and (max-width: 425px) {

            div.quadro {
                padding-left: -1%;
                height: 0;
                margin-left:0;
                display: inline-table;
                padding-top: 60px;
            }
            div#testo-primo-riquadro {
                text-align: center;
                font-size: 80%;
                float: left;
                text-transform: uppercase;
                width: 13%;
                margin-top: 37%;
                line-height: 14px;
                color: #fff;
                font-family: "Open Sans";
                margin-right: 2%;
                margin-left: 15%;
            }
            div#testo-secondo-riquadro {
                text-align: left;
                font-size: 80%;
                float: left;
                display: block;
                width: 50%;
                line-height: 14px;
                margin-top: 35%;
                margin-left: 6%;
                color: #fff;
                font-family: "Open Sans";
            }
            div#testo-primo-riquadro img {
                height: 42px;
                width: 42px;
            }
            img.apice {
                display:none;
            }
            img.apice2 {
                display:none;
            }
            img.pedice {
                display:none;
            }
            img.pedice2 {
                display:none;
            }
            img.prima, img.dopo {
                zoom: 70%;
                top: 45%;
            }
        }

        @media screen and (min-width: 426px) and (max-width: 529px) {

            div.quadro {
                padding-left: -1%;
                height: 0;
                margin-left:10%;
                margin-top:0;
                padding-top:0;
            }
            div#testo-primo-riquadro {
                text-align: center;
                font-size: 80%;
                float: left;
                text-transform: uppercase;
                width: 13%;
                margin-top: 37%;
                line-height: 14px;
                color: #fff;
                font-family: "Open Sans";
                margin-right: 2%;
                margin-left: 7%;
            }
            div#testo-secondo-riquadro {
                text-align: left;
                font-size: 80%;
                float: left;
                display: block;
                width: 19%;
                line-height: 14px;
                margin-top: 29%;
                color: #fff;
                font-family: "Open Sans";
            }
            div#testo-primo-riquadro img {
                height: 80px;
                width: 80px;
            }
            img.apice {
                display:none;
            }
            img.apice2 {
                display:none;
            }
            img.pedice {
                display:none;
            }
            img.pedice2 {
                display:none;
            }
            img.prima, img.dopo {
                zoom: 70%;
            }
        }

        @media screen and (min-width: 530px) and (max-width: 767px) {

            div.quadro {
                margin-left: 10%;
                margin-top: 0;
                padding-top:0;
                height: 0;
            }
            div#testo-secondo-riquadro {
                font-size: 88%;
                line-height: 16px;
                width: 25%;
                margin-top: 30%;
                float: left;
            }
            div#testo-primo-riquadro {
                font-size: 80%;
                width: 10%;
                margin-left: 5%;
                margin-top: 34%;
                margin-bottom: 10%;
            }
            div#testo-primo-riquadro img {
                height: 50px;
                width: 50px;
            }
            img.apice {
                display:none;
            }
            img.apice2 {
                display:none;
            }
            img.pedice {
                display:none;
            }
            img.pedice2 {
                display:none;
            }
            img.prima, img.dopo {
                zoom: 70%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {

            div#testo-secondo-riquadro {
                width: 27%;
                font-size: 100%;
                margin-right: 7.2%;
                line-height: 27px;
                margin-top: 24%;
            }
            div#testo-primo-riquadro {
                margin-left: 0;
                font-size: 100%;
                margin-top: 26%;
                margin-bottom: 10%;
                width: 10%;
            }
            div.quadro {
                margin-left: 14.3%;
                margin-top: -45.8%;
                padding-top: 42%;
            }
            div#testo-primo-riquadro img {
                height: 80px;
                width: 80px;
            }
            img.apice {
                left: 21%;
                top: 14%;
            }
            img.apice2 {
                left: 60.8%;
                top: 14%;
            }
            img.pedice {
                top: 72.7%;
                left: 39.4%;
            }
            img.pedice2 {
                top: 72.7%;
                left: 78.9%;
            }
        }

        @media screen and (min-width: 1025px) and (max-width: 1440px) {

            div.quadro {
                margin-left: 14.3%;
                margin-top: -3.8%;
                padding-top: 0;
                height: 0;
            }
            div#testo-secondo-riquadro {
                font-size: 110%;
                margin-top: 17.5%;
                width: 22%;
            }
            div#testo-primo-riquadro {
                margin-top: 21%;
            }
            div#testo-primo-riquadro img {
                height: 110px;
                width: 110px;
            }
            img.apice {
                left: 26%;
                top: 10%;
            }
            img.apice2 {
                left: 60.8%;
                top: 10%;
            }
            img.pedice {
                top: 72.7%;
                left: 42.4%;
            }
            img.pedice2 {
                top: 72.7%;
                left: 76.1%;
            }
        }
    .swiper-container {
        width: 600px;
        height: 300px;
    }
    </style>
    <?php
        $querys = get_posts(array(
            'post_type'             => 'recensioni',
            'color'                 => 'blue',
            'posts_per_page'        =>  -1,
            'order'                 => 'ASC',
            'orderby'               => 'ASC'
        ));
        ?>
    <?php if ($querys) ;
        { ?>
            <div id="slider" class="swiper-container">
                <ul class="swiper-wrapper">
                    <li class="swiper-slide">
                        <?php $i = 1;
                        foreach ($querys as $query) {
                            if ($i % 2 != 0) {
                                echo '<div class="recensioni">';
                            }?>
                                <img class="apice" src="wp-content/plugins/recensioni-plugin/img/testimonianze-apice.png"/>
                                    <div class="quadro">
                                        <div id="testo-primo-riquadro">
                                            <?php echo get_the_post_thumbnail($query->ID, 'thumbnail'); ?><br/>
                                            <span style="font-weight:bold"><?php echo get_post_meta($query->ID, 'nome', true); ?></span><br/>
                                            <span style="font-weight:bold"><?php echo get_post_meta($query->ID, 'cognome', true) ?></span><br/>
                                            <span style="color: #d1d1d1"><?php echo get_post_meta($query->ID, 'livello', true); ?></span>
                                        </div>
                                        <div id="testo-secondo-riquadro"><?php echo get_post_meta($query->ID, 'commento', true); ?></div>
                                    </div>
                                <img class="pedice" src="wp-content/plugins/recensioni-plugin/img/testimonianze-pedice.png"/>
                            <?php
                            if ($i % 2 == 0) {
                                echo '<img class="apice2" src="wp-content/plugins/recensioni-plugin/img/testimonianze-apice.png"/>';
                                echo '<img class="pedice2" src="wp-content/plugins/recensioni-plugin/img/testimonianze-pedice.png"/>';
                                    echo '</li>';
                            }
                            $i++;
                        } ?>
                    </li>
                        <?php wp_reset_postdata(); ?>
                </ul>
                <img class="prima" src="wp-content/plugins/recensioni-plugin/img/testimonianze-arrow_sx_off.png"/>
                <img class="dopo" src="wp-content/plugins/recensioni-plugin/img/testimonianze-arrow_dx_off.png"/>
            </div>
        <?php
        }
        $myvariable = ob_get_clean();
        return $myvariable;
    }
?>